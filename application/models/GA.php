<?php


class GA extends CI_Model
{


	private $analytics;
	private $analyticsReporting;


	/*
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$KEY_FILE_LOCATION = APPPATH . '/third_party/ga-api.json';

		// Create and configure a new client object.
		$client = new Google_Client();
		$client->setApplicationName("My GA");
		$client->setAuthConfig($KEY_FILE_LOCATION);
		$client->setScopes([
		  		"https://www.googleapis.com/auth/analytics",
		  		"https://www.googleapis.com/auth/analytics.readonly",
		  		"https://www.googleapis.com/auth/analytics.edit",
		  		"https://www.googleapis.com/auth/analytics.manage.users",
		  		"https://www.googleapis.com/auth/analytics.manage.users.readonly"
		  	]);
		$this->analyticsReporting = new Google_Service_AnalyticsReporting($client);
		$this->analytics = new Google_Service_Analytics($client);

	}




	/**
	 * Queries the Analytics Reporting API.
	 *
	 * @return The Analytics Reporting API response.
	 */
	public function getReport() {

	  // Create the DateRange object.
	  $dateRange = new Google_Service_AnalyticsReporting_DateRange();
	  $dateRange->setStartDate("7daysAgo");
	  $dateRange->setEndDate("today");

	  // Create the Metrics object.
	  $sessions = new Google_Service_AnalyticsReporting_Metric();
	  $sessions->setExpression("ga:sessions");
	  $sessions->setAlias("sessions");

	  // Create the ReportRequest object.
	  $request = new Google_Service_AnalyticsReporting_ReportRequest();
	  $request->setViewId(GA_VIEW_ID);
	  $request->setDateRanges($dateRange);
	  $request->setMetrics(array($sessions));

	  $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
	  $body->setReportRequests( array( $request) );
	  return $this->analyticsReporting->reports->batchGet( $body );
	}


	/**
	 * Adds Audience List to Analytics (persona)
	 *
	 * @return Array.
	 */
	public function addAudienceList($name)
	{
		/*
		 * This request creates a new STATE_BASED Remarketing Audience.
		 */
		$linkedAdAccount = new Google_Service_Analytics_LinkedForeignAccount();
		$linkedAdAccount->setType("ADWORDS_LINKS");
		$linkedAdAccount->setLinkedAccountId(GA_LINKED_AD_ACCOUNT_ID);

		// Create the IncludeConditions object.
		$includeConditions = new Google_Service_Analytics_IncludeConditions();
		$includeConditions->setIsSmartList(false);
		$includeConditions->setMembershipDurationDays(30);
		$includeConditions->setSegment("users::condition::ga:browser==Chrome");
		$includeConditions->setDaysToLookBack(30);

		// Create the ExcludeConditions object.
		$excludeConditions = new Google_Service_Analytics_RemarketingAudienceStateBasedAudienceDefinitionExcludeConditions();
		$excludeConditions->setExclusionDuration("PERMANENT");
		$excludeConditions->setSegment("sessions::condition::ga:city==London");

		// Create the AudienceDefinition object.
		$stateBasedAudienceDefinition = new Google_Service_Analytics_RemarketingAudienceStateBasedAudienceDefinition();
		$stateBasedAudienceDefinition->setIncludeConditions($includeConditions);
		$stateBasedAudienceDefinition->setExcludeConditions($excludeConditions);

		//Create the RemarketingAudience object.
		$audience = new Google_Service_Analytics_RemarketingAudience();
		$audience->setName($name);
		$audience->setLinkedViews([GA_VIEW_ID]);
		$audience->setLinkedAdAccounts([$linkedAdAccount]);
		$audience->setAudienceType("STATE_BASED");
		$audience->setStateBasedAudienceDefinition($stateBasedAudienceDefinition);

		try 
		{
		  $resp = $this->analytics->management_remarketingAudience->insert(GA_ACCOUNT_ID, GA_PROPERTY_ID, $audience);
		  return array('status'=>'success', 'response'=>$resp);
		}
		catch (apiServiceException $e)
		{
		    return array('status'=>'error', 'error-code'=>$e->getCode(), 'error-message'=>$e->getMessage());
		}
		catch (apiException $e) {
			return array('status'=>'error', 'error-code'=>$e->getCode(), 'error-message'=>$e->getMessage());
		}
	}


}