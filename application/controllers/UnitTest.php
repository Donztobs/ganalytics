<?php


class UnitTest extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->library('unit_test');
	}


	public function index()
	{
		$this->unit->run($this->multiply(5, 4), 20,"Multiply Func");
		$this->unit->run($this->divide(100, 5), 20,"Divide Func");

		$this->load->view('tests');
	}


	public function multiply($a, $b)
	{
		return $a*$b;
	}

	public function divide($a, $b)
	{
		return $a/$b;
	}
}