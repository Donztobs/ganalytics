<?php



class Analytics extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('GA');

		$reports = $this->GA->getReport();
		$data['reports'] = $reports;
		$this->load->view('analytics', $data);
	}


	public function audience()
	{
		$this->load->model('GA');

		$this->GA->addAudienceList("Persona Name");
	}


	
}