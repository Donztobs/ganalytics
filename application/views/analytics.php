<?php

	for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
	    $report = $reports[ $reportIndex ];
	    $header = $report->getColumnHeader();
	    $dimensionHeaders = $header->getDimensions();
	    $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
	    $rows = $report->getData()->getRows();

	    for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
	      $row = $rows[ $rowIndex ];
	      $dimensions = $row->getDimensions();
	      $metrics = $row->getMetrics();
	      for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
	        print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
	      }

	      for ($j = 0; $j < count($metrics); $j++) {
	        $values = $metrics[$j]->getValues();
	        for ($k = 0; $k < count($values); $k++) {
	          $entry = $metricHeaders[$k];
	          print($entry->getName() . ": " . $values[$k] . "\n");
	        }
	      }
	    }
	  }


?>